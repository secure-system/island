- [ ] This doesn't fall into the followings categories:
    - bugs that only the upstream have the ability to fix. Report them at https://github.com/oasisfeng/island/issues/new/choose. e.g.
      - feature requests
      - bugs related to the code rather than packaging
    - bugs that can't be reproduced elsewhere than your phone

## Summary

<!-- Summarize the bug encountered concisely -->

## Steps to reproduce

<!-- How one can reproduce the issue - this is very important -->

## What is the current bug behavior?

<!-- What actually happens -->

## What is the expected correct behavior?

<!-- What you should see instead -->

## Relevant logs and/or screenshots

<!-- Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise. -->

## Environment information

<!-- Information about Insular and about your device -->
Android version: 
Insular version: 

## Possible fixes

<!-- If you can, link to the line of code that might be responsible for the problem -->

/label ~bug ~needs-investigation